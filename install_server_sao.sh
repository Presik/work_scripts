#!/bin/sh
cd
if ! [ -x "$(command -v node)" ]; then
  curl -sL https://deb.nodesource.com/setup_16.x | sudo bash -
  sudo apt -y install nodejs
  >&2
  echo "node is installed"
fi
curl https://downloads-cdn.tryton.org/6.0/tryton-sao-6.0.25.tgz --output /home/psk/tryton_sao.tgz
tar -xzvf tryton_sao.tgz
cd package
npm install --production --legacy-peer-deps
cd ..
sudo mkdir -p /var/www/localhost
sudo mv package /var/www/localhost/tryton_sao
sudo chown -R psk:psk /var/www/localhost
