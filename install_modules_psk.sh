message "[INFO] Installing psk Tryton packages..."
modules="
    account_co_pyme
    account_col
    account_exo
    account_stock_latin
    account_voucher
    analytic_voucher
    account_bank_statement
    party_personal
    reports
    staff
    staff_co
    staff_loan
    staff_event
    staff_payroll
    staff_payroll_co
    staff_contracting
    staff_access
    staff_access_extratime
    invoice_report
    account_invoice_update
    product_onebarcode
    account_invoice_discount
    product_reference
    product_image
    sale_shop
    sale_salesman
    sale_pos
    sale_cost
    das
    goal
    sale_pos_frontend
    sale_pos_frontend_rest
    sale_contract
    purchase_report
    product_asset_attribute
    account_invoice_discount
    purchase_discount
    analytic_payroll
    electronic_invoice_co
    analytic_report
    staff_payroll_project
    staff_project
    invoice_project
    email
    dash_sale
    dash_stock
    dash
    hotel
    electronic_payroll
    collection
    purchase_co
    sale_co
    
"

for i in ${modules}
    do
        git clone https://presik@bitbucket.org/presik/trytonpsk-$i.git
        cd trytonpsk-$i
        python3 setup.py install
        cd ..
    done
message "[INFO] Done. "
