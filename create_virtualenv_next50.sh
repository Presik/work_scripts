#!/bin/sh

#---------------------------------------------------------
#    Script Install Tryton for Home Intance
# --------------------------------------------------------

# Main functions/variables declaration

# Colors constants
NONE="$(tput sgr0)"
RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
YELLOW="\n$(tput setaf 3)"
BLUE="\n$(tput setaf 4)"

message () {
    # $1 : Message
    # $2 : Color
    # return : Message colorized
    local NOW="[$(date +%H:%M:%S)] "

    echo "${2}${NOW}${1}${NONE}"
}


echo '----------------------------------------------------------'
#read -p 'Introduce the tryton name user: ' nameuser
version='5.0'
max_version='5.1'
integer_version='50'
venv='tryton50'
nameuser='psk'
file_bashrc=${HOME}'/.bashrc'

# Install new apt packages

sudo apt install python3-gi-cairo
sudo apt install python-suds

# Create Virtualenv
message "[INFO] Creating virtualenv... $venv " ${BLUE}

source_="\nalias workon='source ~/.virtualenvs/$venv/bin/activate'"
echo $source_ >> $file_bashrc

virtualenv -p /usr/bin/python3.7 ${HOME}/.virtualenvs/${venv}

message "[INFO] Done." ${YELLOW}
read -p "Was created virtualenv $venv? " response

message "[INFO] Your Virtualenv $venv is ready...!" ${YELLOW}
PYT_CMD=${HOME}/.virtualenvs/${venv}/bin/python3.7

# Install PIP packages
PIP_CMD=${HOME}/.virtualenvs/${venv}/bin/pip3

message "[INFO] Installing main PIP3 packages..." ${BLUE}
pippackages="
    bcrypt
    virtualenv
    pytz
    werkzeug
    wrapt
    qrcode
    cryptography
    pyOpenssl
    polib
    python-stdnum
    jinja2
    python-sql
    zeep
    psycopg2
    psycopg2cffi
    psk_numword
    vobject
    simpleeval
    cached-property
    relatorio
    chardet
    passlib
    requests
    python-magic
    flask
    flask_tryton
    flask_cors
    gunicorn
    Werkzeug==0.15.1
    psycopg2-binary
    wheel
    suds-py3
    beautifulsoup4
    sendgrid
"

for i in ${pippackages}
    do
        ${PIP_CMD} install $i
    done


tryton_modules="
    country
    party
    currency
    company
    product
    stock
    account
    account_product
    account_invoice
    account_invoice_history
    account_statement
    account_invoice_stock
    account_asset
    account_credit_limit
    bank
    account_payment
    product_cost_fifo
    product_cost_history
    product_price_list
    product_attribute
    stock_forecast
    stock_inventory_location
    stock_product_location
    stock_location_sequence
    stock_product_location
    purchase
    purchase_request
    purchase_requisition
    production
    stock_supply
    stock_supply_day
    stock_supply_forecast
    sale
    sale_supply
    sale_opportunity
    sale_price_list
    sale_invoice_grouping
    sale_credit_limit
    analytic_account
    analytic_invoice
    analytic_purchase
    analytic_sale
    account_credit_limit
    commission
    commission_waiting
    production_work
    production_routing
    timesheet
    company_work_time
    account_stock_anglo_saxon
    account_stock_continental
    project
    sale_stock_quantity
"

pip3 install trytond==5.0.21

message "[INFO] Installing official Tryton packages..." ${BLUE}
for i in ${tryton_modules}
do
    ${PIP_CMD} install "trytond_$i==5.0" --no-deps
done
message "[INFO] Done. " ${YELLOW}


message "[INFO] Installing psk Tryton packages..." ${BLUE}
modules="
    account_co_pyme
    account_col
    account_exo
    account_stock_latin
    account_voucher
    analytic_voucher
    account_bank_statement
    party_personal
    company_department
    reports
    staff
    staff_co
    staff_event
    staff_payroll
    staff_payroll_co
    staff_contracting
    staff_access
    staff_access_extratime
    staff_payroll_access
    crm
    ims
    invoice_report
    account_invoice_update
    staff_access_shield
    product_onebarcode
    company_timezone
    company_location
    account_invoice_discount
    product_onebarcode
    product_reference
    product_image
    sale_discount
    sale_shop
    sale_salesman
    sale_pos
    sale_cost
    sale_goal
    sale_pos_frontend
    sale_contract
    analytic_sale_contract
    stock_duration
    stock_product_cost
    stock_shipment_barcode
    purchase_report
    purchase_suggested
    party_card
    maintenance
    hotel
    audiovisual
    engineering
    transport
    staff_document
    document
    document_communication
    document_collection
    notification_document
    product_asset_attribute
    account_invoice_discount
    party_reward_points
    commission_global
    account_invoice_discount_wizard
    invoice_editable_line
    purchase_editable_line
    account_invoice_discount_wizard
    invoice_editable_line
    purchase_discount
    analytic_payroll
    electronic_invoice_co
    analytic_report
    staff_payroll_project
    staff_project
    invoice_project
    dashboard
    email
    real_state
    surveillance
"

for i in ${modules}
    do
        git clone https://presik@bitbucket.org/presik/trytonpsk-$i.git
        cd trytonpsk-$i
        ${PYT_CMD} setup.py install
        cd ..
    done
message "[INFO] Done. " ${YELLOW}


# Changing owner of new virtualenv
message "[INFO] Changing owner of virtualenv... " ${BLUE}
chown $nameuser:$nameuser -R ${HOME}/.virtualenvs/
message "[INFO] Done." ${YELLOW}

#systemctl stop strytond.service
#systemctl disable strytond.service

# Enable:
#systemctl enable strytond.service

# Reboot
