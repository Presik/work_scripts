# Crontab will execute every month first day 

# Add next line to crontab /etc/crontab
# 10 2   1 * *  psk   /home/psk/renew_certificate.sh

# First stop web server nginx
sudo systemctl stop nginx

# Execute renew 
sudo /opt/letsencrypt/certbot-auto renew

# Remove local certificate
rm ~/.certificate/*

# Copy certificate from root path to user home path
sudo cp /etc/letsencrypt/live/cloud10.presik.com/fullchain.pem ~/.certificate
sudo cp /etc/letsencrypt/live/cloud10.presik.com/privkey.pem ~/.certificate

# Change permissions
sudo chown psk:psk ~/.certificate/*

# Restart nginx
sudo systemctl start nginx


# Check certificate expiration

openssl x509 -enddate -noout -in /home/psk/.certificate/fullchain.pem


sudo reboot

