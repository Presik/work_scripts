DROP TABLE "purchase_line_account_tax" CASCADE;
DROP TABLE "purchase_invoice_recreated_rel" CASCADE;
DROP TABLE "account_multirevenue_line" CASCADE;
DROP TABLE "account_invoice_line-stock_move" CASCADE;
DROP TABLE account_invoice_tax CASCADE;
DROP TABLE account_invoice_line_account_tax CASCADE;
DROP TABLE "account_invoice-account_move_line" CASCADE;
DROP TABLE analytic_account_line CASCADE;
DROP TABLE account_tax_line CASCADE;
DROP TABLE account_note_line CASCADE;
DROP TABLE account_note CASCADE;
DROP TABLE account_voucher CASCADE;
DROP TABLE account_voucher_line CASCADE;
DROP TABLE account_asset_line CASCADE;
DROP TABLE account_asset CASCADE;
DROP TABLE account_move CASCADE;
DROP TABLE account_move_line CASCADE;
DROP TABLE purchase_line CASCADE;
DROP TABLE purchase_purchase CASCADE;
DROP TABLE purchase CASCADE;
DROP TABLE sale_line CASCADE;
DROP TABLE sale_sale CASCADE;
DROP TABLE account_invoice_line CASCADE;
DROP TABLE account_invoice CASCADE;
DROP TABLE company_employee CASCADE;
DROP TABLE purchase_request CASCADE;
DROP TABLE purchase_requisition CASCADE;
DROP TABLE staff_liquidation CASCADE;
DROP TABLE staff_liquidation_line CASCADE;
DROP TABLE staff_payroll CASCADE;
DROP TABLE staff_payroll_line CASCADE;
DROP TABLE staff_contract CASCADE;
DROP TABLE staff_shift_line CASCADE;
DROP TABLE staff_event CASCADE;
DROP TABLE staff_contract_futhermore CASCADE;
DROP TABLE staff_payroll_mandatory_wage CASCADE;
DROP TABLE staff_employee_uniform CASCADE;
DROP TABLE staff_uniform_line CASCADE;
DROP TABLE staff_employee_medical_exam CASCADE;
DROP TABLE staff_medical_exam_line CASCADE;
DROP TABLE staff_payroll_electronic_line CASCADE;
DROP TABLE staff_payroll_electronic CASCADE;
DROP TABLE "staff_liquidation_line_adjustment" CASCADE;
DROP TABLE "staff_liquidation_line_move_line_rel" CASCADE;
DROP TABLE payroll_electronic_payroll_period_rel CASCADE;
DROP TABLE payroll_electronic_liquidation_period_rel CASCADE;
DROP TABLE payroll_electronic_line_access_line_rel CASCADE;
DROP TABLE stock_shipment_in CASCADE;
DROP TABLE stock_shipment_in_return CASCADE;
DROP TABLE stock_shipment_out CASCADE;
DROP TABLE stock_shipment_out_return CASCADE;
DROP TABLE "res_user-company_employee" CASCADE;
ALTER TABLE res_user DROP column employee;
UPDATE company_company SET token_api=null;







