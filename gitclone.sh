#!/bin/sh


source ~/.virtualenvs/tryton50/bin/activate

# Define your function here
bitClone () {
 echo "Cloning remote Presik git repo from Bitbucket... $1"
 cd source
 git clone https://git@bitbucket.org/presik/trytonpsk-$1.git
 cd trytonpsk_$1
 python setup.py install
 if [ ! -z $2 ]
 then
	echo "Updating database... $2"
	trytond-admin -v -c ~/.trytond/trytond.conf -d $2 -u $1
 fi
}

# Invoke your function
bitClone "$1" "$2"
