#!/bin/sh

#---------------------------------------------------------
#    Script Install Printer Api 
# --------------------------------------------------------

# Main functions/variables declaration
version='0.0.1'
dir_printer_api=$HOME/.printer_api


mkdir $dir_printer_api


# Install sudo apt packages

echo "[INFO] Installing main sudo apt packages..."
#sudo sudo apt update
#sudo sudo apt -y upgrade
pip3 install -U uvicorn uvloop python-dotenv ujson starlette fastapi aiofiles

echo "[INFO] Done install packages."

# Add download and install printer api module
echo "[INFO] Installing printer api..."

git clone https://developer_presik@bitbucket.org/presik/printer-api.git
mv printer-api $dir_printer_api
cd $dir_printer_api

#touch run_printer.sh
cat > run_printer.sh <<- EOF
#! /bin/bash
    
~/.local/bin/uvicorn --app-dir ~/.printer_api/printer-api main:app --reload --port 7001 --root-path /api
    
EOF

chmod 777 -R $dir_printer_api
cd

echo "[INFO] Done."

echo "Ingresa el usuario: "
read -r user

cd /etc/systemd/system/

sudo bash -c 'cat > printer_api1.service' <<- EOF
[Unit]
Description=PrinterAPI Server
After=network.target

[Service]
User=${user}
WorkingDirectory=/home/${user}/.printer_api/
ExecStart=/home/${user}/.printer_api/run_printer.sh
#ExecStop=

[Install]
WantedBy=multi-user.target

EOF

cd $HOME

sudo systemctl enable printer_api1.service
sudo systemctl stop printer_api1.service
sudo systemctl start printer_api1.service
sudo systemctl status printer_api1.service

echo "[INFO] Done."


