#---------------------------------------------------------
#    Fill this variables before run script               #
                                                         #
domain=domain                                      #
#                                                        #
#---------------------------------------------------------
sudo apt install python3-venv libaugeas0 nginx
sudo python3 -m venv /opt/certbot/
sudo /opt/certbot/bin/pip install --upgrade pip
sudo /opt/certbot/bin/pip install certbot certbot-nginx
sudo ufw allow 'Nginx Full'
sudo ufw reload
sudo ln -s /opt/certbot/bin/certbot /usr/bin/certbot
sudo certbot certonly --nginx
sudo touch /etc/cron.daily/certbot
echo '#!/bin/bash' | sudo tee -a /etc/cron.daily/certbot
echo "certbot renew --dry-run" | sudo tee -a /etc/cron.daily/certbot
sudo chmod 777 /etc/cron.daily/certbot
sudo chown -R psk:psk /etc/letsencrypt/live
sudo chown -R psk:psk /etc/letsencrypt/archive
mkdir .certificate
ln -s /etc/letsencrypt/live/${domain}/privkey.pem /home/psk/.certificate/
ln -s /etc/letsencrypt/live/${domain}/fullchain.pem /home/psk/.certificate/
sudo chown -R psk:psk ~/.certificate
